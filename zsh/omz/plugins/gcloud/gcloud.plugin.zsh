# Google Cloud SDK
# The next line updates PATH for the Google Cloud SDK.
source '/home/mrtux/Applications/google-cloud-sdk/path.zsh.inc'
# The next line enables zsh completion for gcloud.
source '/home/mrtux/Applications/google-cloud-sdk/completion.zsh.inc'
