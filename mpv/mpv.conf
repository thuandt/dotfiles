# mpv config file
# Last Modified : 27/04/2015 04:00 PM

####################
# General settings #
####################

# prepend module name to log messages
msg-module
# color log messages on terminal
msg-color
# display a progress bar on the terminal
term-osd-bar

# Output some video stats
term-playing-msg='Resolution: ${width}x${height}, Framerate: ${fps} Hz'

force-window=yes
cursor-autohide=1000

##################
# video settings #
##################

# Use this for a widescreen monitor, non-square pixels.
monitoraspect=16:9

# force starting with centered window
geometry=50%:50%

# don't allow a new window to have a size larger than 90% of the screen size
autofit-larger=90%x90%

# Keep the player window on top of all other windows.
#ontop=yes

# Keep mpv window open after it finished the playback
keep-open=yes

##########################
# OSD/Subtitles settings #
##########################

# Display Vietnamese subtitles if available.
slang=vi,vie,Vietnamese,en,eng,English

# Play English or Vietnamese audio if available, fall back to otherwise.
alang=en,eng,English

# Load all subs containing media filename.
sub-auto=fuzzy
sub-file-paths=ass:srt:sub:subs:subtitles

# Change subtitle encoding. For Arabic subtitles use 'cp1256'.
# If the file seems to be valid UTF-8, prefer UTF-8.
sub-codepage=utf8:cp1256

# SSA/ASS subtitle rendering
sub-ass

# Enables extraction of Matroska embedded fonts
embeddedfonts=yes

### Subtitles ###
demuxer-mkv-subtitle-preroll
sub-font="Source Sans Pro Semibold"
sub-font-size=50
sub-margin-y=30
sub-color="#FFFFFFFF"
sub-border-color="#FF262626"
sub-border-size=3.2
sub-shadow-offset=1
sub-shadow-color="#33000000"
sub-spacing=0.5
sub-ass-override=yes
sub-ass-force-style="Fontname=Source Sans Pro Semibold,Fontsize=50,PrimaryColour=&H00FFFFFF,BackColour=&H30262626,OutlineColour=&H30262626,Bold=0,Italic=0,Alignment=2,BorderStyle=1,Outline=2.0,Shadow=1,MarginL=20,MarginR=20,MarginV=8,Kerning=yes"

## OSD ##
script-opts=osc-layout=bottombar,osc-seekbarstyle=bar
osd-playing-msg='${filename}'
osd-duration=2000
osd-font="Source Sans Pro Semibold"
osd-font-size=35
osd-color="#ffffffff"
osd-border-color="#ff262626"
osd-border-size=2
osd-shadow-offset=1
osd-shadow-color="#33000000"
osd-scale-by-window=yes

##################
# audio settings #
##################

# Specify default audio driver (see --ao=help for a list).
ao=pulse

# DTS-MA decoding
ad=lavc:libdcadec

# Hide album art
audio-display=no

# Maximum amplification level in percent
volume-max=130

# startup volume
volume=100

##################
# other settings #
##################

# Pretend to be a web browser. Might fix playback with some streaming sites,
# but also will break with shoutcast streams.
user-agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:50.0) Gecko/20100101 Firefox/50.0"

# cache settings
# TFW fell for the 16 GiB RAM meme
cache=auto
cache-default=8000000
cache-initial=1000
cache-seek-min=2000
cache-secs=1000

# Read ahead about 5 seconds of audio and video packets.
#demuxer-readahead-secs=5.0

# Helps play back UHDTV HEVC
#vd-lavc-threads=16

### Video profiles ###
[low]
deband=yes
blend-subtitles=yes
hwdec=vaapi-copy
ytdl=yes
ytdl-format=bestvideo[height<=?720][vcodec!=vp9]+bestaudio/best

[medium]
blend-subtitles=yes
hwdec=vaapi-copy
ytdl=yes
ytdl-format=bestvideo[height<=?1080][vcodec!=vp9]+bestaudio/best

[high]
tscale=oversample
blend-subtitles=yes
interpolation=yes
video-sync=display-resample
hwdec=vaapi-copy
ytdl-format=bestvideo[height<=?1080]+bestaudio/best

[higher]
blend-subtitles=yes
interpolation=yes
tscale=oversample
cscale=ewa_lanczossharp
scale=ewa_lanczossharp
video-sync=display-resample
hwdec=vaapi-copy
ytdl-format=bestvideo+bestaudio/best

[pseudo-gui]
profile=high

# I don't want to wait ages for the window to finally show up before I can move my focus around again
[protocol.http]
force-window=immediate

[protocol.https]
profile=protocol.http

# Audio-only content
[audio]
force-window=no
video=no

## Screenshots ##
screenshot-format=png
screenshot-png-filter=0
screenshot-png-compression=9
screenshot-template="~/Documents/Pictures/Screenshots/%f-%n"
